import 'package:flutter/material.dart';
import 'package:weather_icons/weather_icons.dart';
import 'package:device_preview/device_preview.dart';

Text textDegree = Text(
  "\u2103",
  style: TextStyle(fontSize: 20),
);
void main() {
  runApp(weatherApp());
}

class weatherApp extends StatefulWidget {
  @override
  State<weatherApp> createState() => _weatherAppState();
}

class _weatherAppState extends State<weatherApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}


Widget HomePage(){
  return 
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage('assets/images/weather1.png'),
          // image: NetworkImage(
          //   // 'https://i.pinimg.com/originals/ee/c4/16/eec4169c5e89189f59d86f21829e0454.jpg',
          //   'assets/images/weather1.png',
          // ),
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: Text(
            "เมืองชลบุรี",
            style: TextStyle(fontSize: 25),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.more_vert),
              onPressed: () {},
            ),
          ],
        ),
        body: ListView(
          children: [
            Container(
                // color: Colors.amber,
                child: Column(
              children: [
                Container(
                    // color: Colors.pink,
                    width: 300,
                    height: 180,
                    // alignment: Alignment.bottomLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            const Text(
                              "25",
                              style: TextStyle(
                                fontSize: 90,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text(
                              "\u2103",
                              style: TextStyle(
                                fontSize: 30,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        )
                      ],
                    )),
                Container(
                  // color: Colors.black,
                  alignment: Alignment.bottomCenter,
                  child: const Text("มีเมฆบางส่วน 21~30\u2103",
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontWeight: FontWeight.w800)),
                ),
                Container(
                  // color: Colors.amber,
                  height: 90,
                ),
                // Divider(
                //   color: Colors.grey,
                //   thickness: 2,
                // ),
              ],
            )
                // color: Colors.amber,
                ),
            Container(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    for (int i = 1; i <= 12; i++) buildWeatherNowIcon(i)
                  ],
                ),
              ),
            ),
            Container(
              height: 30,
            ),
            Container(
              color: Colors.white.withOpacity(0.3),
              child: Column(
                children: [
                  buildDetailIcon(),
                  buildDetailIcon(),
                  buildDetailIcon(),
                  buildDetailIcon(),
                  buildDetailIcon(),
                  buildDetailIcon(),
                  buildDetailIcon()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

Widget buildWeatherNowIcon(var i) {
  if (i < 10) {
    i = "0$i";
  }
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: Icon(
            Icons.sunny,
            // WeatherIcons.night_snow,
            // color: Colors.white,
            color: Colors.yellow,
            size: 30,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "$i:00",
            style: TextStyle(color: Colors.black, fontSize: 17),
          ),
        ),
        Container(
          height: 10,
        )
      ],
    ),
  );
}

Widget buildDetailIcon() {
  return Padding(
    padding: const EdgeInsets.all(15.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "27/12",
          style: TextStyle(color: Colors.black, fontSize: 18),
        ),
        Text(
          "วันนี้",
          style: TextStyle(color: Colors.grey, fontSize: 18),
        ),
        Icon(
          WeatherIcons.day_snow_thunderstorm,
          color: Colors.blue,
          size: 30,
        ),
        Text(
          "ต่ำสุด",
          style: TextStyle(color: Colors.grey, fontSize: 18),
        ),
        Text(
          "สูงสุด",
          style: TextStyle(
              color: Colors.black, fontSize: 18, fontFamily: 'Helvetica'),
        ),
      ],
    ),
  );
}













// DevicePreview(
//       tools: const [
//         DeviceSection(),
//       ],
//       builder: (context) => MaterialApp(
//         debugShowCheckedModeBanner: false,
//         useInheritedMediaQuery: true,
//         builder: DevicePreview.appBuilder,
//         locale: DevicePreview.locale(context),
//         title: 'Responsive and adaptive UI in Flutter',
//         theme: ThemeData(
//           primarySwatch: Colors.green,
//         ),
//         home: Scaffold(
//           backgroundColor: Colors.white,
//           appBar: AppBar(
//             title: const Text('App Title'),
//           ),
//           body: AspectRatioExample(),
//         ),
//       ),
//     );